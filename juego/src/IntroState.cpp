#include "IntroState.h"

#include "../include/RecordsState.h"
#include "PlayState.h"
#include "../include/CreditosState.h"
#include "DificultadState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
  _camera = _sceneMgr->createCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0.01, 20, 0));
  _camera->lookAt(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(100);
  _camera->setFOVy(Ogre::Degree(50));
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  crearMenu();

  _menuSeleccion = 0;
  cambiarTexturaMenu();

  _exitGame = false;
}

void
IntroState::exit()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{

  return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
  //transicion al PlayState
  if (e.key == 28 && _menuSeleccion == 0) {
    changeState(DificultadState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 1) {
	  changeState(RecordsState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 2) {
  	  changeState(CreditosState::getSingletonPtr());
    }

  if (e.key == 28 && _menuSeleccion == 3) {
      _exitGame = true;
    }

  if (e.key == OIS::KC_UP && _menuSeleccion > 0){
  	  _menuSeleccion--;
  	  cambiarTexturaMenu();
    }

  if (e.key == OIS::KC_DOWN && _menuSeleccion < 3){
	  _menuSeleccion++;
	  cambiarTexturaMenu();
  }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void IntroState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, -10, 0);

	//Nodo Jugar
	Ogre::SceneNode *node_jugar = _sceneMgr->createSceneNode("Jugar");
	Ogre::Entity* ent_jugar = _sceneMgr->createEntity("Jugar", "Plane.mesh");
	node_jugar->attachObject(ent_jugar);
	node_menu->addChild(node_jugar);
	node_jugar->translate(-7.5, 0, 0);
	node_jugar->yaw(Ogre::Degree(90));

	//Nodo Records
	Ogre::SceneNode *node_records = _sceneMgr->createSceneNode("Records");
	Ogre::Entity* ent_records = _sceneMgr->createEntity("Records", "Plane.mesh");
	node_records->attachObject(ent_records);
	node_menu->addChild(node_records);
	node_records->translate(-2.5, 0, 0);
	node_records->yaw(Ogre::Degree(90));
	ent_records->setMaterialName("MaterialMenuA2");

	//Nodo Creditos
	Ogre::SceneNode *node_creditos = _sceneMgr->createSceneNode("Creditos");
	Ogre::Entity* ent_creditos = _sceneMgr->createEntity("Creditos", "Plane.mesh");
	node_creditos->attachObject(ent_creditos);
	node_menu->addChild(node_creditos);
	node_creditos->translate(2.5, 0, 0);
	node_creditos->yaw(Ogre::Degree(90));
	ent_creditos->setMaterialName("MaterialMenuA3");

	//Nodo Salir
	Ogre::SceneNode *node_salir = _sceneMgr->createSceneNode("Salir");
	Ogre::Entity* ent_salir = _sceneMgr->createEntity("Salir", "Plane.mesh");
	node_salir->attachObject(ent_salir);
	node_menu->addChild(node_salir);
	node_salir->translate(7.5, 0, 0);
	node_salir->yaw(Ogre::Degree(90));
	ent_salir->setMaterialName("MaterialMenuA4");
}



void IntroState::cambiarTexturaMenu(){
	switch (_menuSeleccion){
	case 0:
		_sceneMgr->getEntity("Jugar")->setMaterialName("MaterialMenuR1");
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenuA2");
		break;
	case 1:
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenuR2");
		_sceneMgr->getEntity("Jugar")->setMaterialName("MaterialMenuA1");
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenuA3");
		break;
	case 2:
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenuR3");
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenuA2");
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenuA4");
		break;
	case 3:
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenuR4");
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenuA3");
		break;
	}
}

IntroState*
IntroState::getSingletonPtr ()
{
return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
