#include "Salvavidas.h"
#include <OgreSceneNode.h>

Salvavidas::Salvavidas(SceneNode* node) {
	_node = node;
}

Salvavidas::Salvavidas(const Salvavidas &obj){
	_node = obj.getNode();
}

Salvavidas::~Salvavidas() {
	delete _node;
}

Salvavidas& Salvavidas::operator= (const Salvavidas &obj){
	delete _node;
	_node = obj._node;
	return *this;
}

SceneNode* Salvavidas::getNode()const{
	return _node;
}
