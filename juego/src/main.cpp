#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "WinState.h"
#include "LoseState.h"
#include "RecordsState.h"
#include "CreditosState.h"
#include "DificultadState.h"

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  WinState* winState = new WinState();
  LoseState* loseState = new LoseState();
  RecordsState* recordsState = new RecordsState();
  CreditosState* creditosState = new CreditosState();
  DificultadState* dificultadState = new DificultadState();

  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(winState);
  UNUSED_VARIABLE(loseState);
  UNUSED_VARIABLE(recordsState);
  UNUSED_VARIABLE(creditosState);
  UNUSED_VARIABLE(dificultadState);

  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
