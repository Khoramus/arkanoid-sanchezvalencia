#include "WinState.h"
#include "PlayState.h"

template<> WinState* Ogre::Singleton<WinState>::msSingleton = 0;

void WinState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(0.01, 20, 0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	crearMenu();

	_exitGame = false;
}

void WinState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyEntity("Continuar");
	_sceneMgr->destroySceneNode("Continuar");
	_sceneMgr->destroyEntity("PlanoMenu");
	_sceneMgr->destroySceneNode("PlanoMenu");
	_sceneMgr->destroySceneNode("Menu");

}

void WinState::pause() {
}

void WinState::resume() {
}

bool WinState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool WinState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void WinState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == 28) {
		changeState(PlayState::getSingletonPtr());
	}
}

void WinState::keyReleased(const OIS::KeyEvent &e) {
}

void WinState::mouseMoved(const OIS::MouseEvent &e) {
}

void WinState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void WinState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

void WinState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, -10, 0);

	//Nodo Continuar
	Ogre::SceneNode *node_continuar = _sceneMgr->createSceneNode("Continuar");
	Ogre::Entity* ent_continuar = _sceneMgr->createEntity("Continuar", "Plane.mesh");
	node_continuar->attachObject(ent_continuar);
	node_menu->addChild(node_continuar);
	node_continuar->yaw(Ogre::Degree(90));
	ent_continuar->setMaterialName("MaterialMenuR8");
	node_continuar->setScale(1.5, 1, 1.5);
}

WinState*
WinState::getSingletonPtr() {
	return msSingleton;
}

WinState&
WinState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

